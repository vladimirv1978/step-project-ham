let start = 0;
let clientsList = [
  [
    "./Image/Client_feedback/Image1.png",
    "Ann Connar",
    "Web Designer",
    "1",
    " Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio egetaliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
  ],
  [
    "./Image/Client_feedback/Image2.png",
    "Bruce Willis",
    "SEO Ingener",
    "2",
    "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Doloremque, rerum!",
  ],
  [
    "./Image/Client_feedback/Image3.png",
    "Jason Statham",
    "UX Designer",
    "3",
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, veritatis!",
  ],
  [
    "./Image/Client_feedback/Image4.png",
    "Debora Smith",
    "Web Designer",
    "4",
    "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Vero nulla ullam odio pariatur, debitis a?",
  ],
  [
    "./Image/Client_feedback/Image5.png",
    "Amelia Brown",
    "Web Designer",
    "5",
    "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Doloremque, rerum!",
  ],
  [
    "./Image/Client_feedback/Image6.png",
    "Emily Walker",
    "UX Designer",
    "6",
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, veritatis!",
  ],
  [
    "./Image/Client_feedback/Image7.png",
    "Jessica Harris",
    "Web Designer",
    "7",
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio molestiae corrupti cum praesentium perspiciatis! Id excepturi ab corporis? Vel, id.",
  ],
  [
    "./Image/Client_feedback/Image8.png",
    "Lily Young",
    "UX Designer",
    "8",
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, veritatis!",
  ],
];
let copy = clientsList.slice();
let copyReverse = clientsList.slice().reverse();
let newArr = clientsList.slice();
let rewindBack = "main__feedback-client-list-rewind-back";
let rewindForward = "main__feedback-client-list-rewind-forward";

function setListenerRewind() {
  let listIcon = [
    ...document.querySelector(".main__feedback-client-list").children,
  ];

  listIcon.forEach(function (elem) {
    elem.addEventListener("click", function (event) {
      if (
        event.target.className == rewindBack ||
        event.target.className == rewindForward
      ) {
        let activeBackground = [
          ...document.querySelectorAll(
            ".main__feedback-client-photo-background"
          ),
        ];

        let indexActiveBackground = activeBackground.findIndex((elem) => {
          return (
            elem.classList[1] == "main__feedback-client-photo-background-active"
          );
        });

        createDomHtml(event.target, indexActiveBackground);
      } else {
        avatarClient(event.target);
      }
    });
  });
}

function createDomHtml(element, indexActiveBackground) {
  let client = document.querySelector(
    ".main__feedback-client-list-rewind-forward-wrapper"
  );
  let imageCarrousel = document.querySelectorAll(
    ".main__feedback-client-photo-wrapper"
  );
  imageCarrousel.forEach((element) => element.remove());

  if (element.className == rewindBack) start = start - 1;
  if (element.className == rewindForward) start = start + 1;

  if (start > newArr.length - 4) newArr = newArr.concat(copy);
  if (start < 0) {
    copyReverse.forEach(function (elem) {
      newArr.unshift(elem);
      start = copyReverse.length - 1;
    });
  }

  for (let i = start; i <= start + 3; i++) {
    client.insertAdjacentHTML(
      "beforebegin",
      `<div class="main__feedback-client-photo-wrapper">
        <img id="${newArr[i][3]}" src="${newArr[i][0]}" onclick="avatarClient(this)" class="main__feedback-client-list-photo" alt="Client photo"/>
         <svg class="main__feedback-client-photo-background" width="70" height="70">
          <circle cx="35" cy="35" r="35" />
        </svg>
      </div>`
    );
  }

  let photoBackground = [
    ...document.querySelectorAll(".main__feedback-client-photo-background"),
  ];
  photoBackground[indexActiveBackground].classList.add(
    "main__feedback-client-photo-background-active"
  );
  startProfile = photoBackground[indexActiveBackground].previousElementSibling;

  avatarClient(startProfile);
}

function avatarClient(avatar) {
  if (avatar.className == rewindBack || avatar.className == rewindForward)
    return;

  let activeBackground = document.querySelector(
    ".main__feedback-client-photo-background-active"
  );
  activeBackground.classList.remove(
    "main__feedback-client-photo-background-active"
  );
  activeBackground.parentElement.classList.remove(
    "main__feedback-client-photo-wrapper-active-position"
  );

  avatar.nextElementSibling.classList.add(
    "main__feedback-client-photo-background-active"
  );
  avatar.parentElement.classList.add(
    "main__feedback-client-photo-wrapper-active-position"
  );

  let image = document.querySelector(".main__feedback-client-photo");
  image.remove();

  let sibling = document.querySelector(".main__feedback-client-profile");
  sibling.insertAdjacentHTML(
    "afterend",
    `<img src="${avatar.src}" class="main__feedback-client-photo animation-avatar-foto"/>`
  );

  let name = document.querySelector(".main__feedback-client-name");
  let profession = document.querySelector(".main__feedback-client-profession");

  for (let i = 0; i <= clientsList.length - 1; i++) {
    let clientIndex = clientsList.findIndex((elem) => elem[i] == avatar.id);
    if (clientIndex !== -1) {
      name.textContent = clientsList[clientIndex][1];
      profession.textContent = clientsList[clientIndex][2];

      let previousDescription = document.querySelector(
        ".main__feedback-description"
      );
      previousDescription.remove();

      let clientProfileField = document.querySelector(
        ".main__feedback-client-profile"
      );
      clientProfileField.insertAdjacentHTML(
        "beforebegin",
        `<p class="main__feedback-description animation-description">${clientsList[clientIndex][4]}</p>`
      );
    }
  }
}

setListenerRewind();
