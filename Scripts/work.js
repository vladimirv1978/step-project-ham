document.querySelector(".main__works-examples").children;
let mainTabs = [...document.querySelectorAll(".main__works-tabs-item")];
let arrImage = [];
let count = 0;

function arrSrcImages() {
  for (let i = 1; i <= 9; i++) {
    for (let j = 0; j <= 3; j++) {
      let arrElem = `./Image/Our amazing Works/${j}_${i}.jpg`;
      arrImage.push(arrElem);
    }
  }
}

function setListenerTabs() {
  document
    .querySelector(".main__works-tabs")
    .addEventListener("click", function (event) {
      let activeTab = event.target.dataset.name;
      activeTabStyle(event.target);
      filterImage(activeTab);
    });
}

function activeTabStyle(element) {
  mainTabs.forEach((elem) =>
    elem.classList.remove("main__works-tabs-item-active")
  );

  element.classList.add("main__works-tabs-item-active");
}

function filterImage(datasetName) {
  let worksImage = [
    ...document.querySelector(".main__works-examples").children,
  ];
  worksImage.forEach((elem) => (elem.hidden = true));

  if (datasetName == "All") worksImage.forEach((elem) => (elem.hidden = false));

  worksImage.forEach((elem) => {
    if (elem.dataset.name == datasetName) {
      elem.hidden = false;
    }
  });
}

function setListenerButton() {
  document
    .querySelector(".main__work-button")
    .addEventListener("click", function () {
      count = count + 1;
      addImage(count);
    });
}

function addImage(count) {
  let worksExamples = document.querySelector(".main__works-examples");
  let diapasonArrImage = [];
  if (count > 1) {
    let button = document.querySelector(".main__work-button");
    button.remove();
  }

  if (count == 0) diapasonArrImage = arrImage;
  if (count == 1) diapasonArrImage = arrImage.slice(12);
  if (count == 2) diapasonArrImage = arrImage.slice(24);

  for (let i = 0; i <= 11; i++) {
    let categoryName;
    let category = String(arrImage[i]).slice(-7, -6);

    switch (category) {
      case "0":
        categoryName = "Graphic Design";
        break;
      case "1":
        categoryName = "Web Design";
        break;
      case "2":
        categoryName = "Landing Pages";
        break;
      case "3":
        categoryName = "Wordpress";
        break;
    }

    let createNewDomElement = document.createElement("img");
    createNewDomElement.src = `${diapasonArrImage[i]}`;
    createNewDomElement.dataset.name = `${categoryName}`;
    createNewDomElement.alt = "product";
    worksExamples.append(createNewDomElement);

    createNewDomElement.addEventListener("mouseenter", function (eventImage) {
      let createElementReplacement = document.createElement("div");
      createElementReplacement.className = "main__works-examples-replacement";
      let replacementIcons = document.createElement("div");
      replacementIcons.className = "main__works-examples-replacement-icons";
      createElementReplacement.prepend(replacementIcons);

      let iconsConnection = document.createElement("a");
      iconsConnection.className =
        "main__works-examples-replacement-icons-connection";
      iconsConnection.href = "#";
      replacementIcons.prepend(iconsConnection);

      let imageIconConnectionCircle = document.createElement("img");
      imageIconConnectionCircle.src =
        "./Image/Icons/Main_work/Ellipse 1 copy 3.png";
      iconsConnection.prepend(imageIconConnectionCircle);

      let imageIconConnectionChain = document.createElement("img");
      imageIconConnectionChain.src =
        "./Image/Icons/Main_work/Combined shape 7431.png";
      imageIconConnectionCircle.after(imageIconConnectionChain);

      let iconsStop = document.createElement("a");
      iconsStop.className = "main__works-examples-replacement-icons-stop";
      iconsStop.href = "#";
      iconsConnection.after(iconsStop);

      let imageIconStopCircle = document.createElement("img");
      imageIconStopCircle.src = "./Image/Icons/Main_work/Ellipse 1.png";
      iconsStop.prepend(imageIconStopCircle);

      let imageIconStopChain = document.createElement("img");
      imageIconStopChain.src = "./Image/Icons/Main_work/Layer 23.png";
      imageIconStopCircle.after(imageIconStopChain);

      let replacementTitle = document.createElement("p");
      replacementTitle.className = "main__works-examples-replacement-title";
      replacementTitle.innerHTML = "creative design";
      replacementIcons.after(replacementTitle);

      let replacementCategory = document.createElement("p");
      replacementCategory.className =
        "main__works-examples-replacement-category";
      replacementCategory.innerHTML = `${categoryName}`;
      replacementTitle.after(replacementCategory);

      eventImage.target.replaceWith(createElementReplacement);
      createElementReplacement.addEventListener("mouseleave", function (event) {
        event.target.replaceWith(eventImage.target);

      });
    });
  }

  let [activeFiltr] = [
    ...document.querySelectorAll(".main__works-tabs-item-active"),
  ];
  filterImage(activeFiltr.dataset.name);
}

arrSrcImages();
addImage(0);
setListenerTabs();
setListenerButton();
