
let tabs = [...document.querySelectorAll(".main__service-tabs-item")];
let tabsContent = [...document.querySelectorAll(".main__service-description-wrapper > li")];

function activeElementDisplay(activeTabElem) {

  tabs.forEach(function(elem){
    elem.classList.remove("main__service-activ-tabs");
    let [img] = elem.children;
    img.classList.remove("main__service-tabs-item-image-active");
  }) 
  tabsContent.forEach((elem) => (elem.hidden = true));

  activeTabElem.classList.add("main__service-activ-tabs");
  
  let [triangle] = activeTabElem.children;
  triangle.classList.add("main__service-tabs-item-image-active");

  let [content] = tabsContent.filter((elem) => elem.dataset.name == activeTabElem.dataset.name);
  content.hidden = false;
}

function addListenerTabs (){
  
  tabs.forEach(function(elem){
    elem.addEventListener("click", function(event){
      if (event.target != this) return;
      activeElementDisplay(event.target);
    })
  })
}

activeElementDisplay(tabs[0]);
addListenerTabs();