let activeBlock = [...document.querySelectorAll(".main__news-image-item")];

function setListenerBlocs() {
    
  activeBlock.forEach(function (elem) {
    elem.addEventListener("mouseenter", function (event) {
      let elementsBlock = [...event.target.children];

      elementsBlock[1].classList.add("main__news-data-post-changestyle");
      elementsBlock[2].classList.add("main__news-image-title-changestyle");
    });
  });

  activeBlock.forEach(function (elem) {
    elem.addEventListener("mouseleave", function (event) {
      let elementsBlock = [...event.target.children];

      elementsBlock[1].classList.remove("main__news-data-post-changestyle");
      elementsBlock[2].classList.remove("main__news-image-title-changestyle");
    });
  });
}

setListenerBlocs();
